//
// Created by admin on 29.12.2021.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_MASTER_MEM_DEBUG_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_MASTER_MEM_DEBUG_H

#include "mem_internals.h"

void debug_block(struct block_header* b, const char* fmt, ... );

void debug(const char* fmt, ... );

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_MASTER_MEM_DEBUG_H
