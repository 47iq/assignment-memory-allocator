//
// Created by admin on 27.12.2021.
//

#define _DEFAULT_SOURCE

#include "test.h"

#include <unistd.h>

#include "mem.h"
#include "mem_debug.h"
#include "mem_internals.h"
#include "util.h"


#define HEAP_SIZE 10000
#define MALLOC_SIZE 1000

static void* heap;
static struct block_header* heap_start;
static void* allocated_block;

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(
    struct block_header, contents));
}

void test_init() {
    debug("\n --- Инициализация кучи ---\n");
    heap = heap_init(HEAP_SIZE);
    if(heap) {
        heap_start = (struct block_header*) (heap);
        debug("Куча инициализирована:\n");
        debug_heap(stderr, heap);
        return;
    }
    err("Не получилось выделить кучу.");
}

void test_allocate() {
    debug("\n --- Тест 1: выделение блока ---\n");
    void* mem = _malloc(MALLOC_SIZE);
    if (mem == NULL)
        err("Ошибка при выделении памяти: Null pointer.");
    if(heap_start->capacity.bytes != MALLOC_SIZE)
        err("Ошибка при выделении памяти: размер выделенной памяти не соответствует запрошенному.");
    if(heap_start->is_free)
        err("Ошибка при выделении памяти: блок не помечен как занятый.");
    debug("\nПамять успешно выделена:\n");
    debug_heap(stderr, heap);
    allocated_block = mem;
}

void test_free_one() {
    debug("\n --- Тест 2: освобождение блока ---\n");
    if(allocated_block == NULL)
        err("Ошибка при очистке памяти: тест очистки памяти должен быть запущен после успешного прохождения теста выделения памяти.");
    _free(allocated_block);
    if(!heap_start->is_free)
        err("Ошибка при очистке памяти: блок не помечен как свободный.");
    debug("\nПамять успешно очищена. Состояние кучи:\n");
    debug_heap(stderr, heap);
}

void test_free_two() {
    debug("\n --- Тест 3: выделение и освобождение 2 блоков ---\n");
    void* first = _malloc(MALLOC_SIZE);
    void* second = _malloc(MALLOC_SIZE);
    debug("\nПамять успешно выделена. Состояние кучи:\n");
    debug_heap(stderr, heap);
    _free(first);
    _free(second);
    if(!heap_start->is_free)
        err("Ошибка при очистке памяти: блок не помечен как свободный.");
    debug("\nПамять успешно очищена. Состояние кучи:\n");
    debug_heap(stderr, heap);
}

void test_heap_grow_extending() {
    debug("\n --- Тест 4: расширение кучи с выделением региона последовательно ---\n");
    const size_t SZ = 2 * HEAP_SIZE;
    void* mem = _malloc(SZ);
    debug_heap(stderr, heap);
    if (mem == NULL)
        err("Ошибка при выделении памяти: Null pointer.");
    struct block_header *last = block_get_header(mem);
    if(last->capacity.bytes != SZ)
        err("Ошибка при выделении памяти: размер выделенной памяти не соответствует запрошенному.");
    if(last->is_free)
        err("Ошибка при выделении памяти: блок не помечен как занятый.");
    _free(mem);
    if(!last->is_free)
        err("Ошибка при очистке памяти: блок не помечен как свободный.");
    debug("\nПамять успешно очищена. Состояние кучи:\n");
    debug_heap(stderr, heap);
}

void test_heap_grow() {
    debug("\n --- Тест 5: расширение кучи с выделением региона в другом месте ---\n");
    const size_t SZ = HEAP_SIZE * 4;
    struct block_header* iter = heap_start;
    if(!iter) {
        err("Ошибка: тесты должны быть запущены последовательно.");
        return;
    }
    struct block_header* last = iter;
    while(iter) {
        last = iter;
        iter = iter->next;
    }
    void* addr = (uint8_t*) last + size_from_capacity(last->capacity).bytes;
    addr = map_pages(addr, 1000,  MAP_FIXED_NOREPLACE);
    debug("\nАдрес, по которому память стала недоступна: %p.\n", addr);
    void* mem = _malloc(SZ);
    struct block_header* new = block_get_header(mem);
    if (mem == NULL)
        err("Ошибка при выделении памяти: Null pointer.");
    if(!new)
        err("Ошибка при выделении памяти: блок не встроен в кучу.");
    if(new->capacity.bytes != SZ)
        err("Ошибка при выделении памяти: размер выделенной памяти не соответствует запрошенному.");
    if(new->is_free)
        err("Ошибка при выделении памяти: блок не помечен как занятый.");
    debug_heap(stderr, heap);
    _free(mem);
    if(!new->is_free)
        err("Ошибка при очистке памяти: блок не помечен как свободный.");
    debug("\nПамять успешно очищена. Состояние кучи:\n");
    debug_heap(stderr, heap);
}
