//
// Created by admin on 27.12.2021.
//

#include "test.h"


int main() {
    test_init();
    test_allocate();
    test_free_one();
    test_free_two();
    test_heap_grow_extending();
    test_heap_grow();

    return 0;
}
